import {SAFlag} from "@components/icons/SAFlag";
import {USFlag} from "@components/icons/USFlag";

export const siteSettings = {
    name: "LE & LO KIDS  Производители детской одежды. Оптовая торговля.",
    description:
        "На этом сайте вы можете найти классную одежду для своих детей.",
    author: {
        name: "lelokids",
        websiteUrl: "lelokids.ru"
    },
    logo: {
        url: "/assets/images/logo.png",
        alt: "Le & lo Kids",
        href: "/",
        width: 70,
        height: 70,
    },
    defaultLanguage: "en",
    currencyCode: "USD",
    site_header: {
        menu: [
            {
                id: 1,
                path: "/terms-of-cooperation",
                label: "О Компании",
                columns: [
                    {
                        id: 1,
                        columnItems: [
                            {
                                id: 1,
                                path: "/terms-of-cooperation",
                                label: "Условия сотрудничества",
                            },
                            {
                                id: 2,
                                path: "/delivery-and-payment",
                                label: "Доставка и оплата",
                            }
                        ],
                    },
                ],
            },
            {
                id: 2,
                path: "/search?q=for-boys",
                label: "Для мальчиков",
                columns: [
                    {
                        id: 1,
                        columnItems: [
                            {
                                id: 1,
                                path: "/search?q=casual-wear",
                                label: "Повседневная одежда",
                                columnItemItems: [
                                    {
                                        id: 1,
                                        path: "/search?q=jumpers-and-long-sleeves",
                                        label: "Джемперы и лонгсливы",
                                    },
                                    {
                                        id: 2,
                                        path: "/search?q=underwear",
                                        label: "Нижнее бельё",
                                    },
                                    {
                                        id: 3,
                                        path: "/search?q=pajamas",
                                        label: "Пижамы",
                                    },
                                    {
                                        id: 4,
                                        path: "/search?q=thermal-underwear",
                                        label: "Термобельё",
                                    },
                                    {
                                        id: 5,
                                        path: "/search?q=hoodies",
                                        label: "Толстовки",
                                    },
                                    {
                                        id: 6,
                                        path: "/search?q=t-shirts",
                                        label: "Футболки",
                                    },
                                    {
                                        id: 7,
                                        path: "/search?q=shorts",
                                        label: "Шорты",
                                    },
                                ],
                            }
                        ],
                    },
                    {
                        id: 2,
                        columnItems: [
                            {
                                id: 1,
                                path: "/search?q=formal-wear",
                                label: "Торжественная одежда",
                                columnItemItems: [
                                    {
                                        id: 1,
                                        path: "/search?q=trousers",
                                        label: "Брюки",
                                    },
                                    {
                                        id: 2,
                                        path: "/search?q=suits",
                                        label: "Костюмы",
                                    },
                                ],
                            }
                        ],
                    }
                ],
            },
            {
                id: 3,
                path: "/search?q=for-girls",
                label: "Для девочек",
                columns: [
                    {
                        id: 1,
                        columnItems: [
                            {
                                id: 1,
                                path: "/search?q=casual-wear",
                                label: "Повседневная одежда",
                                columnItemItems: [
                                    {
                                        id: 1,
                                        path: "/search?q=breeches",
                                        label: "Бриджи",
                                    },
                                    {
                                        id: 2,
                                        path: "/search?q=jumpers-and-long-sleeves",
                                        label: "Джемперы и лонгсливы",
                                    },
                                    {
                                        id: 3,
                                        path: "/search?q=kits",
                                        label: "Комплекты",
                                    },
                                    {
                                        id: 4,
                                        path: "/search?q=leggings",
                                        label: "Легинсы",
                                    },
                                    {
                                        id: 5,
                                        path: "/search?q=underwear",
                                        label: "Нижнее бельё",
                                    },
                                    {
                                        id: 6,
                                        path: "/search?q=pajamas",
                                        label: "Пижамы",
                                    },
                                    {
                                        id: 7,
                                        path: "/search?q=dresses",
                                        label: "Платья",
                                    },
                                    {
                                        id: 8,
                                        path: "/search?q=hoodies",
                                        label: "Толстовки",
                                    },
                                    {
                                        id: 9,
                                        path: "/search?q=t-shirts",
                                        label: "Футболки",
                                    }
                                ],
                            }
                        ],
                    },
                    {
                        id: 2,
                        columnItems: [
                            {
                                id: 1,
                                path: "/search?q=formal-wear",
                                label: "Торжественная одежда",
                                columnItemItems: [
                                    {
                                        id: 1,
                                        path: "/search?q=trousers",
                                        label: "Брюки",
                                    },
                                    // {
                                    //     id: 2,
                                    //     path: "/search?q=suits",
                                    //     label: "Костюмы",
                                    // },
                                ],
                            }
                        ],
                    }
                ],
            },
            {
                id: 4,
                path: "/contact-us",
                label: "Контакты",
            },
        ],
        mobileMenu: [
            {
                id: 1,
                path: "/search?q=about-company",
                label: "О Компании",
                subMenu: [
                    {
                        id: 1,
                        path: "/search?q=terms-of-cooperation",
                        label: "Условия сотрудничества",
                    },
                    {
                        id: 2,
                        path: "/search?q=delivery-and-payment",
                        label: "Доставка и оплата",
                    }
                ],
            },
            {
                id: 2,
                path: "/search?q=for-boys",
                label: "Для мальчиков",
                subMenu: [

                    {
                        id: 1,
                        path: "/search?q=jumpers-and-long-sleeves",
                        label: "Джемперы и лонгсливы",
                    },
                    {
                        id: 2,
                        path: "/search?q=underwear",
                        label: "Нижнее бельё",
                    },
                    {
                        id: 3,
                        path: "/search?q=pajamas",
                        label: "Пижамы",
                    },
                    {
                        id: 4,
                        path: "/search?q=thermal-underwear",
                        label: "Термобельё",
                    },
                    {
                        id: 5,
                        path: "/search?q=hoodies",
                        label: "Толстовки",
                    },
                    {
                        id: 6,
                        path: "/search?q=t-shirts",
                        label: "Футболки",
                    },
                    {
                        id: 7,
                        path: "/search?q=shorts",
                        label: "Шорты",
                    },
                    {
                        id: 8,
                        path: "/search?q=trousers",
                        label: "Брюки",
                    },
                    {
                        id: 9,
                        path: "/search?q=suits",
                        label: "Костюмы",
                    },
                ]
            },
            {
                id: 3,
                path: "/search?q=for-girls",
                label: "Для девочек",
                subMenu: [
                    {
                        id: 1,
                        path: "/search?q=breeches",
                        label: "Бриджи",
                    },
                    {
                        id: 2,
                        path: "/search?q=jumpers-and-long-sleeves",
                        label: "Джемперы и лонгсливы",
                    },
                    {
                        id: 3,
                        path: "/search?q=kits",
                        label: "Комплекты",
                    },
                    {
                        id: 4,
                        path: "/search?q=leggings",
                        label: "Легинсы",
                    },
                    {
                        id: 5,
                        path: "/search?q=underwear",
                        label: "Нижнее бельё",
                    },
                    {
                        id: 6,
                        path: "/search?q=pajamas",
                        label: "Пижамы",
                    },
                    {
                        id: 7,
                        path: "/search?q=dresses",
                        label: "Платья",
                    },
                    {
                        id: 8,
                        path: "/search?q=hoodies",
                        label: "Толстовки",
                    },
                    {
                        id: 9,
                        path: "/search?q=t-shirts",
                        label: "Футболки",
                    },
                    {
                        id: 10,
                        path: "/search?q=trousers",
                        label: "Брюки",
                    },
                ],
            },
            {
                id: 4,
                path: "/contact-us",
                label: "Контакты",
            },
        ],
        languageMenu: [
            {
                id: "ar",
                name: "عربى - AR",
                value: "ar",
                icon: <SAFlag width="20px" height="15px"/>,
            },
            {
                id: "en",
                name: "English - EN",
                value: "en",
                icon: <USFlag width="20px" height="15px"/>,
            }
        ],
    },
};

// import isEmpty from "lodash/isEmpty";

interface Item {
    _id: string;
    title: string;
    slug: string;
    photos: any;
    price: number;
    salePrice?: number;
    description?: string;

    // [key: string]: unknown;
}

export function generateCartItem(item: Item) {
    const {_id, title, slug, photos, price, salePrice, description} = item;
    return {
        id: _id,
        title,
        slug,
        photos: photos?.[0],
        price: salePrice ? salePrice : price,
        description
    };
}

import React from "react";


const Certificate = () => {
    return (
        <div className="py-7">
            <div>
                <p className="text-2xl font-bold">Добро пожаловать на официальный сайт LE & LO KIDS</p>
                <p>
                    Детская одежда LE&LO это 100% экологически чистый узбекский хлопок. Мягкий трикотаж не вызывает
                    аллергии
                    и очень комфортен в ношении.
                </p>
                <p>
                    Модные фасоны, яркие расцветки и красочные принты детской одежды LE&LO очень нравятся детям.
                    Современные
                    краски устойчивы к выцветанию и выгоранию, не боятся частых стирок. МЫ ПРЕДЛАГАЕМ: 100% качественный
                    товар.
                </p>
                <p>
                    Товары сертифицированы и маркированы по требованиям законодательства ТС. Вся продукция поставляется
                    официально, предоставляем все необходимые документы для торговли на территории РФ.
                </p>
            </div>
            <div className="py-4">
                <p className="text-2xl font-bold pb-5">
                    Вы можете купить наши товары в следующих магазинах
                </p>
                <div className="grid grid-cols-1 md:grid-cols-4 md:flex md:justify-evenly">

                    <div className="flex justify-center items-center shadow-cart px-4">
                        <a href="https://www.wildberries.ru/seller/79886">
                            <img width={300} src="/assets/images/wildberry.png" alt=""/>
                        </a>
                    </div>
                    <div className="shadow-cart px-4">
                        <a href="https://www.ozon.ru/seller/lelo-kids-130207/odezhda-obuv-i-aksessuary-7500/?miniapp=seller_130207">
                            <img width={300}  src="/assets/images/ozon.png" alt=""/>
                        </a>
                    </div>

                    {/*<div>*/}
                    {/*    <a href="https://happywear.ru/brands/le-lo">*/}
                    {/*        <img src="/assets/images/happywear.png" alt=""/>*/}
                    {/*    </a>*/}
                    {/*</div>*/}
                    {/*<div>*/}
                    {/*    <a href="https://to-toshka.ru/le-and-lo/z">*/}
                    {/*        <img src="/assets/images/totoshka.jpeg" alt=""/>*/}
                    {/*    </a>*/}
                    {/*</div>*/}
                </div>
            </div>

            {/*<p className="text-2xl font-bold text-center my-8">Наши сертификаты</p>*/}
            {/*<div className="grid grid-cols-1 md:grid-cols-2 justify-around">*/}
            {/*    <div>*/}
            {/*        <img src="/assets/images/sert1.jpeg" alt=""/>*/}
            {/*    </div>*/}
            {/*    <div>*/}
            {/*        <img src="/assets/images/sert2.jpeg" alt=""/>*/}
            {/*    </div>*/}
            {/*</div>*/}
        </div>
    )
}

export default Certificate;

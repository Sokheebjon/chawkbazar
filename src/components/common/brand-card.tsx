// import Link from "next/link";
import Image from "next/image";
// import {Brand} from "@framework/types";
// import {ROUTES} from "@utils/routes";
import {useTranslation} from "next-i18next";
import {useUI} from "@contexts/ui.context";

const BrandCard: React.FC<{ brand: any }> = ({brand}) => {
    // const {title, photos}: any = brand?.product;
    const {t} = useTranslation("common");
    const {openModal, setModalView, setModalData} = useUI();

    function handlePopupView() {
        setModalData({data: brand?.product});
        setModalView("PRODUCT_VIEW");
        return openModal();
    }

    return (
        <div
            onClick={handlePopupView}
            role="button"
        >
            <a className="group flex justify-center text-center relative overflow-hidden rounded-md">
                <Image
                    src={brand?.product?.photos[0] ?? "/assets/placeholder/brand-bg.svg"}
                    alt={brand?.product?.title || t("text-brand-thumbnail")}
                    width={428}
                    height={428}
                    className="rounded-md object-cover transform transition-transform ease-in-out duration-500 group-hover:rotate-6 group-hover:scale-125"
                />
                <div
                    className="absolute top left bg-black w-full h-full opacity-50 transition-opacity duration-500 group-hover:opacity-80"/>
                <div className="absolute top left text-white h-full w-full flex items-center justify-center p-8">
                    <div>
                        <p className="text-3xl font-bold">{brand?.product?.title}</p>
                        {/*<p>{price}</p>*/}
                    </div>
                    {/*<img*/}
                    {/*    src={photos[0]}*/}
                    {/*    alt={title || t("text-brand-thumbnail")}*/}
                    {/*    className="flex-shrink-0"*/}
                    {/*/>*/}
                </div>
            </a>
        </div>
    );
};

export default BrandCard;

import Input from "@components/ui/input";
import PasswordInput from "@components/ui/password-input";
import Button from "@components/ui/button";
import {useForm} from "react-hook-form";
import {useLoginMutation, LoginInputType} from "@framework/auth/use-login";
import {useUI} from "@contexts/ui.context";
import Logo from "@components/ui/logo";
// import {ImGoogle2, ImFacebook2} from "react-icons/im";
import {useTranslation} from "next-i18next";
import {useRouter} from "next/router";

const LoginForm: React.FC = () => {
    const {t} = useTranslation();
    const router = useRouter();
    const {setModalView, openModal, closeModal} = useUI();
    const {mutate: login, isLoading} = useLoginMutation();

    const {
        register,
        handleSubmit,
        formState: {errors},
    } = useForm<LoginInputType>();

    function onSubmit({email, password}: LoginInputType) {
        login({
            email,
            password
        });
        router.back();
    }

    function handleSignUp() {
        setModalView("SIGN_UP_VIEW");
        return openModal();
    }

    // function handleForgetPassword() {
    //     setModalView("FORGET_PASSWORD");
    //     return openModal();
    // }

    return (
        <div
            className="overflow-hidden bg-white mx-auto rounded-lg w-full sm:w-96 md:w-450px border border-gray-300 py-5 px-5 sm:px-8">
            <div className="text-center mb-6 pt-2.5">
                <div onClick={closeModal}>
                    <Logo/>
                </div>
            </div>
            <form
                onSubmit={handleSubmit(onSubmit)}
                className="flex flex-col justify-center"
                noValidate
            >
                <div className="flex flex-col space-y-3.5">
                    <Input
                        labelKey="Эл. адрес"
                        type="email"
                        variant="solid"
                        {...register("email", {
                            required: `${t("forms:email-required")}`,
                            pattern: {
                                value:
                                    /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                                message: t("forms:email-error"),
                            },
                        })}
                        errorKey={errors.email?.message}
                    />
                    <PasswordInput
                        labelKey="Пароль"
                        errorKey={errors.password?.message}
                        {...register("password", {
                            required: `${t("forms:password-required")}`,
                        })}
                    />
                    {/*<div className="flex items-center justify-center">*/}
                    {/*    <div className="flex ms-auto">*/}
                    {/*        <button*/}
                    {/*            type="button"*/}
                    {/*            onClick={handleForgetPassword}*/}
                    {/*            className="text-end text-sm text-heading ps-3 underline hover:no-underline focus:outline-none"*/}
                    {/*        >*/}
                    {/*            {t("Забыл пароль?")}*/}
                    {/*        </button>*/}
                    {/*    </div>*/}
                    {/*</div>*/}
                    <div className="relative">
                        <Button
                            type="submit"
                            loading={isLoading}
                            disabled={isLoading}
                            className="h-11 md:h-12 w-full mt-1.5"
                        >
                            Логин
                        </Button>
                    </div>
                </div>
            </form>
            <div className="flex flex-col items-center justify-center relative text-sm text-heading mt-6 mb-3.5">
                <hr className="w-full border-gray-300"/>
                {/*<span className="absolute -top-2.5 px-2 bg-white">*/}
                {/*	{t("common:text-or")}*/}
                {/*</span>*/}
            </div>
            <div className="text-sm sm:text-base text-body text-center mt-5 mb-1">
                У вас нет аккаунта?
                <button
                    type="button"
                    className="text-sm sm:text-base text-heading underline font-bold hover:no-underline focus:outline-none"
                    onClick={handleSignUp}
                >
                    Регистр
                </button>
            </div>
        </div>
    );
};

export default LoginForm;

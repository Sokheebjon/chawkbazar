import React, {useEffect, useState} from "react";
// import Counter from "@components/common/counter";
// import { useRouter } from "next/router";
// import {useProductQuery} from "@framework/product/get-product";
// import {getVariations} from "@framework/utils/get-variations";
import usePrice from "@framework/product/use-price";
import {useCart} from "@contexts/cart/cart.context";
import {generateCartItem} from "@utils/generate-cart-item";
// import { ProductAttributes } from "./product-attributes";
// import isEmpty from "lodash/isEmpty";
// import Link from "@components/ui/link";
import {toast} from "react-toastify";
import {useWindowSize} from "@utils/use-window-size";
import Carousel from "@components/ui/carousel/carousel";
import {SwiperSlide} from "swiper/react";
import ProductMetaReview from "@components/product/product-meta-review";
import {useRouter} from "next/router";
import {useQuery} from "react-query";
import http from "@framework/utils/http";
import {Form, Input, Space, Button as AntdButton, Select, Tooltip, Row, Col} from "antd";
import {useForm} from "antd/lib/form/Form";
import {CloseCircleOutlined, PlusOutlined} from "@ant-design/icons"
import Button from "@components/ui/button";

const productGalleryCarouselResponsive = {
    "1025": {
        slidesPerView: 1
    },
    "768": {
        slidesPerView: 2,
    },
    "0": {
        slidesPerView: 1,
    },
};


const ProductSingleDetails: React.FC = () => {
    const [form] = useForm();
    // const {
    // 	query: { slug },
    // } = useRouter();
    const {query} = useRouter();
    const [formState, setFormState] = useState<any>();
    const {
        data,
        isLoading
    } = useQuery(["get-single-product-by-id"], async () => http.get(`/product/${query?.slug}`), {enabled: !!query?.slug})
    const {width} = useWindowSize();

    const {addItemToCart} = useCart();
    // const [quantity, setQuantity] = useState(1);
    const quantity = formState?.reduce((accumulator: any, currentValue: any) => {
        return accumulator + parseFloat(currentValue?.quantity)
    }, 0);

    const [addToCartLoader, setAddToCartLoader] = useState<boolean>(false);
    const {price, basePrice, discount} = usePrice(
        data && {
            amount: data?.data?.salePrice ? data?.data?.salePrice : data?.data?.price,
            baseAmount: data?.data?.price,
            currencyCode: "RUB",
        }
    )

    const ageOptions = data?.data?.ages?.map((item: string) => (
        {
            label: `${item} лет`,
            value: item
        }
    ))

    const colorOptions = data?.data?.colors?.map((item: string) => {
        const colorName = item?.split("&")?.[0]
        const colorCode = item?.split("&")?.[1]

        return {
            label: <div className="flex justify-between items-center">
                <span>{colorName}</span>
                <div style={{width: 20, height: 20, backgroundColor: colorCode}}/>
            </div>,
            value: item
        }
    })

    const handleFormChange = (changed: any, values: any) => {
        setFormState(values?.products);
        console.log(changed);
    };

    useEffect(() => {
        const detailList = data?.data?.ages?.map((age: string) => {
            return {
                eachPack: data?.data?.eachPack,
                age,
                price: data?.data?.price
            }
        })
        if (detailList) {
            form.setFieldsValue({products: [detailList?.[0]]})
        }
    }, [data])

    const onFinish = (values: any) => {
        console.log('Received values of form:', values);
    };


    if (isLoading) return <p>Loading...</p>;

    // const variations = getVariations(data?.variations);
    // const isSelected = !isEmpty(variations)
    //     ? !isEmpty(attributes) &&
    //     Object.keys(variations).every((variation) =>
    //         attributes.hasOwnProperty(variation)
    //     )
    //     : true;

    function addToCart() {
        // if (!isSelected) return;
        // to show btn feedback while product carting
        if (formState?.length === 0) {
            return toast.error("Пожалуйста, выберите цвет и возраст");
        }
        setAddToCartLoader(true);
        setTimeout(() => {
            setAddToCartLoader(false);
        }, 600);

        const item = generateCartItem(data?.data!);
        addItemToCart({...item, order: formState}, quantity);
        toast("Добавлено в корзину", {
            type: "dark",
            progressClassName: "fancy-progress-bar",
            position: width > 768 ? "bottom-right" : "top-right",
            autoClose: 2000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
        });
    }

    // function handleAttribute(attribute: any) {
    // 	setAttributes((prev) => ({
    // 		...prev,
    // 		...attribute,
    // 	}));
    // }

    return (
        <div className="block lg:grid grid-cols-8 gap-x-10 xl:gap-x-14 pt-7 pb-10 lg:pb-14 2xl:pb-20 items-start">
            {width < 1025 ? (
            <Carousel
                pagination={{
                    clickable: true,
                }}
                breakpoints={productGalleryCarouselResponsive}
                className=""
                buttonClassName="hidden"
            >
                {data?.data?.photos?.map((item: any, index: number) => (
                    <SwiperSlide key={`product-gallery-key-${index}`}>
                        <div className="col-span-1 transition duration-150 ease-in hover:opacity-90">
                            <img
                                src={item}
                                alt={`${data?.data?.title}--${index}`}
                                className="object-cover w-full"
                            />
                        </div>
                    </SwiperSlide>
                ))}
            </Carousel>
            ) : (
                <div className="col-span-4 grid grid-cols-2 gap-2.5">
                    {data?.data?.photos?.map((item: any, index: number) => (
                        <div
                            key={index}
                            className="col-span-1 transition duration-150 ease-in hover:opacity-90"
                        >
                            <img
                                src={item}
                                alt={`${data?.data?.title}--${index}`}
                                className="object-cover w-full"
                            />
                        </div>
                    ))}
                </div>
            )}

            <div className="col-span-4 pt-8 lg:pt-0">
                <div className="pb-7 mb-7 border-b border-gray-300">
                    <h2 className="text-heading text-lg md:text-xl lg:text-2xl 2xl:text-3xl font-bold hover:text-black mb-3.5">
                        {data?.data?.title}
                    </h2>
                    <p className="text-body text-sm lg:text-base leading-6 lg:leading-8">
                        {data?.data?.description}
                    </p>
                    <div className="flex items-center mt-5">
                        <div
                            className="text-heading font-bold text-base md:text-xl lg:text-2xl 2xl:text-4xl pe-2 md:pe-0 lg:pe-2 2xl:pe-0">
                            {price}
                        </div>
                        {discount && (
                            <span
                                className="line-through font-segoe text-gray-400 text-sm md:text-base lg:text-lg xl:text-xl ps-2">
								{basePrice}
							</span>
                        )}
                    </div>
                </div>
                {/*<div className="flex justify-start mb-5">*/}
                {/*    {data?.data?.colors?.map((color: string) => (*/}
                {/*        <div className="form-check">*/}
                {/*            <input style={{backgroundColor: color, width: 25, height: 25}}*/}
                {/*                   onChange={(e) => handleColorChange(e, color)}*/}
                {/*                   className="form-check-input appearance-none h-4 w-4 border border-gray-300 rounded-sm bg-white checked:scale-y-150 focus:outline-none transition duration-200 mt-1 align-top bg-no-repeat bg-center bg-contain float-left mr-2 cursor-pointer"*/}
                {/*                   type="checkbox" value="" id="flexCheckDefault"/>*/}
                {/*        </div>*/}
                {/*    ))}*/}
                {/*</div>*/}
                <div className="overflow-x-auto w-full scroll-smooth">
                    <Form form={form} requiredMark={false} layout="vertical"
                          onFinish={onFinish} onValuesChange={handleFormChange}>
                        <Form.List name="products">
                            {(fields, {add, remove}) => (
                                <>
                                    {fields.map(({key, name, ...restField}) => (
                                        <Space key={key} style={{
                                            display: 'flex',
                                            justifyContent: "space-between",
                                            alignItems: "start",
                                            marginBottom: 8
                                        }} align="baseline" className="border-b border-blue-400 pb-5">
                                            <Row gutter={[10, 10]}>
                                                <Col span={10}>
                                                    <Form.Item
                                                        {...restField}
                                                        name={[name, 'age']}
                                                        label="Выберите возраст"
                                                        rules={[{
                                                            required: true,
                                                            message: 'Пожалуйста, выберите возраст'
                                                        }]}
                                                    >
                                                        <Select options={ageOptions}/>
                                                    </Form.Item>
                                                </Col>
                                                <Col span={6}>
                                                    <Form.Item
                                                        {...restField}
                                                        name={[name, 'eachPack']}
                                                        label="В упак."
                                                        initialValue={data?.data?.eachPack}
                                                    >
                                                        <Input disabled/>
                                                    </Form.Item>
                                                </Col>
                                                <Col span={8}>
                                                    <Form.Item
                                                        {...restField}
                                                        name={[name, 'color']}
                                                        label="Выберите цвет"
                                                        rules={[{required: true, message: 'Пожалуйста, выберите цвет'}]}
                                                    >
                                                        <Select style={{width: 200}} options={colorOptions}/>
                                                    </Form.Item>
                                                </Col>
                                                <Col span={12}>
                                                    <Form.Item
                                                        {...restField}
                                                        name={[name, 'quantity']}
                                                        label="Напишите количество"
                                                        rules={[{required: true, message: 'Выберите количество'}]}
                                                    >
                                                        <Input type="number"/>
                                                    </Form.Item>
                                                </Col>
                                                <Col span={12}>
                                                    <Form.Item
                                                        {...restField}
                                                        name={[name, 'price']}
                                                        label="Цена каждого продукта"
                                                        initialValue={data?.data?.price}
                                                    >
                                                        <Input suffix="₽" disabled/>
                                                    </Form.Item>
                                                </Col>
                                            </Row>
                                            <Tooltip placement="top" title={"Закрывать"}>
                                                <CloseCircleOutlined
                                                    className="my-auto ml-5"
                                                    style={{fontSize: 20}}
                                                    onClick={() => remove(name)}
                                                />
                                            </Tooltip>
                                        </Space>
                                    ))}
                                    <Form.Item>
                                        <AntdButton type="primary" id={"addBtn"} onClick={() => add()} block
                                                    icon={<PlusOutlined/>}>
                                            Добавить поля
                                        </AntdButton>
                                    </Form.Item>
                                </>
                            )}
                        </Form.List>
                    </Form>
                </div>

                {/*<div className="flex justify-start">*/}
                {/*    {data?.data?.ages?.map((age: string) => (*/}
                {/*        <div className="form-check mr-4">*/}
                {/*            <input*/}
                {/*                onChange={(e) => handleAgeChange(e, age)}*/}
                {/*                className="form-check-input appearance-none h-4 w-4 border border-gray-300 rounded-sm bg-white checked:bg-blue-600 checked:border-blue-600 focus:outline-none transition duration-200 mt-1 align-top bg-no-repeat bg-center bg-contain float-left mr-2 cursor-pointer"*/}
                {/*                type="checkbox" value="" id={`ageCheck${age}`}/>*/}
                {/*            <label className="form-check-label inline-block text-gray-800"*/}
                {/*                   htmlFor={`ageCheck${age}`}>*/}
                {/*                {age} лет*/}
                {/*            </label>*/}
                {/*        </div>*/}
                {/*    ))}*/}
                {/*</div>*/}
                <div className="pb-3 border-b border-gray-300">
                    {/*{Object.keys(variations).map((variation) => {*/}
                    {/*	return (*/}
                    {/*		<ProductAttributes*/}
                    {/*			key={variation}*/}
                    {/*			title={variation}*/}
                    {/*			attributes={variations[variation]}*/}
                    {/*			active={attributes[variation]}*/}
                    {/*			onClick={handleAttribute}*/}
                    {/*		/>*/}
                    {/*	);*/}
                    {/*})}*/}
                </div>
                <div
                    className="flex items-center space-s-4 md:pe-32 lg:pe-12 2xl:pe-32 3xl:pe-48 border-b border-gray-300 py-8">
                    {/*<Counter*/}
                    {/*    quantity={quantity}*/}
                    {/*    onIncrement={() => setQuantity((prev) => prev + 1)}*/}
                    {/*    onDecrement={() =>*/}
                    {/*        setQuantity((prev) => (prev !== 1 ? prev - 1 : 1))*/}
                    {/*    }*/}
                    {/*    disableDecrement={quantity === 1}*/}
                    {/*/>*/}
                    <Button
                        onClick={addToCart}
                        variant="slim"
                        className={"bg-gray-400 hover:bg-gray-400"}
                        // disabled={!isSelected}
                        loading={addToCartLoader}
                    >
                        <span className="py-2 3xl:px-8">Добавить в корзину</span>
                    </Button>
                </div>
                {/*<div className="py-6">*/}
                {/*    <ul className="text-sm space-y-5 pb-1">*/}
                {/*        <li>*/}
                {/*			<span className="font-semibold text-heading inline-block pe-2">*/}
                {/*				SKU:*/}
                {/*			</span>*/}
                {/*            {data?.sku}*/}
                {/*        </li>*/}
                {/*        <li>*/}
                {/*			<span className="font-semibold text-heading inline-block pe-2">*/}
                {/*				Category:*/}
                {/*			</span>*/}
                {/*            <Link*/}
                {/*                href="/"*/}
                {/*                className="transition hover:underline hover:text-heading"*/}
                {/*            >*/}
                {/*                {data?.category?.name}*/}
                {/*            </Link>*/}
                {/*        </li>*/}
                {/*        {data?.tags && Array.isArray(data.tags) && (*/}
                {/*            <li className="productTags">*/}
                {/*				<span className="font-semibold text-heading inline-block pe-2">*/}
                {/*					Tags:*/}
                {/*				</span>*/}
                {/*                {data?.tags?.map((tag) => (*/}
                {/*                    <Link*/}
                {/*                        key={tag.id}*/}
                {/*                        href={tag.slug}*/}
                {/*                        className="inline-block pe-1.5 transition hover:underline hover:text-heading last:pe-0"*/}
                {/*                    >*/}
                {/*                        {tag.name}*/}
                {/*                        <span className="text-heading">,</span>*/}
                {/*                    </Link>*/}
                {/*                ))}*/}
                {/*            </li>*/}
                {/*        )}*/}
                {/*    </ul>*/}
                {/*</div>*/}

                <ProductMetaReview data={data}/>
            </div>
        </div>
    );
};

export default ProductSingleDetails;
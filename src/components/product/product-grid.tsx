import ProductCard from "@components/product/product-card";
// import Button from "@components/ui/button";
import type {FC} from "react";
// import {useProductsQuery} from "@framework/product/get-all-products";
import {useRouter} from "next/router";
import {useQuery} from "react-query";
import http from "@framework/utils/http";
import ProductFeedLoader from "@components/ui/loaders/product-feed-loader";

// import { useTranslation } from "next-i18next";
interface ProductGridProps {
    className?: string;
}

export interface ProductType {
    price: number
    salePrice?: number
    description?: string;
    title: string;
    subcategoryId?: string
    categoryId?: string;
    photos?: Array<string>,
    _id?: string
}

export const ProductGrid: FC<ProductGridProps> = ({className = ""}) => {
    const {query}: any = useRouter();
    const priceFilters = query?.price?.split(",");

    const url = (query?.q === "boys" || query?.q === "girls") ? `/products/:/${query?.q}` : `/products/${query?.q}/:`

    const prices = priceFilters?.reduce((accumulator: any, currentValue: string) => {
        for (let i = 0; i < currentValue?.split("-")?.length; i++) {
            accumulator.push(currentValue?.split("-")?.[i]);
        }
        return accumulator?.flat()?.map((item: string) => {
            if (item !== "") {
                return parseFloat(item);
            }
        });
    }, [])

    const from = prices?.filter((item: any) => !isNaN(item))?.reduce(function (p: any, v: any) {
        return (p < v ? p : v);
    })
    const to = prices?.filter((item: any) => !isNaN(item))?.reduce(function (p: any, v: any) {
        return (p > v ? p : v);
    })

    const getAllProductByCategory: any = useQuery(["get-all-products-by-category", from, to, query?.q], async () => http.get(url, {
        params: {
            from,
            to: to === 100000 ? "" : to
        }
    }), {enabled: !!query?.q})

    // const { t } = useTranslation("common");

    return (
        <>
            <div
                className={`grid grid-cols-2 sm:grid-cols-3 xl:grid-cols-4 2xl:grid-cols-5 gap-x-3 lg:gap-x-5 xl:gap-x-7 gap-y-3 xl:gap-y-5 2xl:gap-y-8 ${className}`}
            >
                {
                    getAllProductByCategory?.isLoading && !getAllProductByCategory?.data?.data ?
                        <ProductFeedLoader limit={20} uniqueKey="search-product"/> :
                        getAllProductByCategory?.data?.data?.length === 0 ? <div>
                                <h3 className="m-auto text-2xl">Нет продукта</h3>
                            </div> :
                            getAllProductByCategory?.data?.data?.map((product: ProductType) => (
                                <ProductCard
                                    key={`product--key${product._id}`}
                                    product={product}
                                    variant="grid"
                                />
                            ))
                }
                {/*{*/}
                {/*	isLoading && !data?.length ? (*/}
                {/*	<ProductFeedLoader limit={20} uniqueKey="search-product" />*/}
                {/*) :*/}
                {/*    {getAllProductByCategory?.map((product) => (*/}
                {/*			<ProductCard*/}
                {/*				key={`product--key${product.id}`}*/}
                {/*				product={product}*/}
                {/*				variant="grid"*/}
                {/*			/>*/}
                {/*		));*/}
                {/*	})*/}
                {/*)}*/}
            </div>
            <div className="text-center pt-8 xl:pt-14">
                {/*{hasNextPage && (*/}
                {/*	<Button*/}
                {/*		loading={loadingMore}*/}
                {/*		disabled={loadingMore}*/}
                {/*		onClick={() => fetchNextPage()}*/}
                {/*		variant="slim"*/}
                {/*	>*/}
                {/*		{t("button-load-more")}*/}
                {/*	</Button>*/}
                {/*)}*/}
            </div>
        </>
    );
};

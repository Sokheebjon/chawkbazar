import React from 'react';
import {useTable, useSortBy, useExpanded} from 'react-table';

export default function Table(props: any) {

    const {
        getTableProps,
        getTableBodyProps,
        headerGroups,
        rows,
        prepareRow,
    } = useTable({columns: props.columns, data: props.data}, useSortBy, useExpanded)

    return (
        <div>
            <div>
                <table {...getTableProps()}
                       className="w-full text-center my-10 ">
                    {props.header !== false ?
                        <thead>
                        {headerGroups.map(headerGroup => (
                            <tr {...headerGroup.getHeaderGroupProps()}
                                className="border border-collapse border-gray-500">
                                {headerGroup.headers.map((column: any) => (
                                    <th className="border border-collapse border-gray-500 sm:text-sm" {...column.getHeaderProps(column.sortBy === true ? column.getSortByToggleProps() : '')}
                                        width={column.Width}>
                                        {column.render('Header')}
                                    </th>
                                ))}
                            </tr>
                        ))}
                        </thead>
                        : ''}
                    <tbody {...getTableBodyProps()} >
                    {rows.map((row: any, i) => {
                        prepareRow(row)
                        return (
                            <React.Fragment key={i}>
                                <tr onClick={props.onClick ? () => props.onClick(row) : null} {...row.getRowProps()}
                                    tabIndex={0} className="border border-collapse border-gray-500">
                                    {row.cells.map((cell: any) => {
                                        return (
                                            <td {...cell.getCellProps()}
                                                className="border border-collapse border-gray-500">
                                                {cell.render('Cell')}
                                            </td>
                                        )
                                    })}
                                </tr>
                            </React.Fragment>
                        )
                    })}
                    </tbody>
                </table>
            </div>
        </div>
    )
}
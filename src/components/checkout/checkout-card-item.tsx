import {Item} from "@contexts/cart/cart.utils";
import {generateCartItemName} from "@utils/generate-cart-item-name";
import usePrice from "@framework/product/use-price";
import {Table} from "antd";

export const CheckoutItem: React.FC<{ item: Item }> = ({item}) => {
    const {price} = usePrice({
        amount: item.itemTotal,
        currencyCode: "RUB",
    });
    const columns = [
        {
            title: '№',
            dataIndex: 'index',
            key: 'index',
            render: (__: string, _: any, index: number) => index + 1
        },
        {
            title: 'Возраст',
            dataIndex: 'age',
            key: 'age',
            render: (__: string, record: any, _: number) => <span>{record?.age} лет</span>
        },
        {
            title: 'Цвет',
            dataIndex: 'color',
            key: 'color',
            render: (__: string, record: any, _: number) => {
                const colorName = record?.color?.split("&")[0];
                const colorCode = record?.color?.split("&")[1];
                return (<div style={{width: 200}} className="flex justify-between">
                    <p>{colorName}</p>
                    <div style={{width: 20, height: 20, backgroundColor: colorCode}}/>
                </div>)
            }
        },
        {
            title: 'Количество',
            dataIndex: 'quantity',
            key: 'quantity',
            // render: (__: string, record: any, _: number) => <span>{record?.age} лет</span>
        },
        {
            title: 'Цена',
            dataIndex: 'price',
            key: 'price',
            // render: (__: string, record: any, _: number) => <span>{record?.age} лет</span>
        },
    ]

    return (
        <div>
            <div className="flex py-4 items-center lg:px-3">
                <div className="flex  rounded-md  w-16 h-16 flex-shrink-0">
                    <img
                        src={item.photos ?? "/assets/placeholder/order-product.svg"}
                        width="64"
                        height="64"
                        className="object-cover"
                    />
                </div>
                <h6 className="text-sm ps-3 font-regular text-heading">
                    {generateCartItemName(item.title, item.attributes)}
                </h6>
                <div className="flex ms-auto text-heading text-sm ps-2 font-semibold flex-shrink-0">
                    {price}
                </div>
            </div>
            <div className="flex justify-between items-start my-2">
                <Table className="w-full" columns={columns} dataSource={item?.order} pagination={false}/>
            </div>
        </div>
    );
};

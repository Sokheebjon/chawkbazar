import usePrice from "@framework/product/use-price";
import {useCart} from "@contexts/cart/cart.context";
import {CheckoutItem} from "@components/checkout/checkout-card-item";
import {CheckoutCardFooterItem} from "./checkout-card-footer-item";
import {useTranslation} from "next-i18next";
import Button from "@components/ui/button";
import {useUI} from "@contexts/ui.context";
import {toast} from "react-toastify";
import {useRouter} from "next/router";
import http from "@framework/utils/http";
import {useMutation, useQuery} from "react-query";
import Cookies from "js-cookie";
import {useEffect, useState} from "react";
// Email
import emailjs from '@emailjs/browser';

const CheckoutCard: React.FC = () => {
    const {items, total, isEmpty} = useCart();
    const [discount, setDiscount] = useState(0)
    const salesList = useQuery(["get-all-sale-list"], async () => http.get(`/admin/sales`))
    const {price: subtotal} = usePrice({
        amount: total,
        currencyCode: "RUB",
    });
    const suitableSale = salesList?.data?.data?.sort((a: any, b: any) => a?.amount - b?.amount)
    useEffect(() => {
        if (suitableSale?.[0]?.amount < total && suitableSale?.[1]?.amount > total) {
            setDiscount(suitableSale?.[0]?.percent)
        } else if (suitableSale?.[1]?.amount < total && suitableSale?.[2]?.amount > total) {
            setDiscount(suitableSale?.[1]?.percent)
        } else if (suitableSale?.[2]?.amount < total && suitableSale?.[3]?.amount > total) {
            setDiscount(suitableSale?.[2]?.percent)
        } else if (suitableSale?.[3]?.amount < total) {
            setDiscount(suitableSale?.[3]?.percent)
        }
    }, [suitableSale, discount])

    const sale = ((100 - discount) * total) / 100
    const {price: salePrice} = usePrice({
        amount: sale,
        currencyCode: "RUB",
    });
    const orderMutate = useMutation((data: any) => http.post(`/products/check-out`, data))
    const router = useRouter()

    const {
        isAuthorized
    } = useUI();
    console.log(items?.[0]?.title, "items")

    const handleSubmitOrder = () => {
        const data = {
            orders: items,
            totalPrice: total,
            salePrice: sale,
            userId: Cookies.get("userId")
        }

        if (!isAuthorized) {
            toast.warning("Пожалуйста, авторизируйтесь перед заказом")
            router.push("/signup")
        } else {
            const orderAsync = orderMutate.mutateAsync(data)
            orderAsync.then((res) => {
                toast.success("Ваш заказ принят")
                console.log(res)
            }).catch((err) => {
                toast.error("Ошибка в заказе")
                console.log(err)
            })
            emailjs.send('gmail', 'template_kcihy7c', {
                title: items?.[0]?.title,
                description: items?.[0]?.description
            }, 'trRy1-5tN9akueAe2')
                .then((result: any) => {
                    console.log(result.text);
                }, (error: any) => {
                    console.log(error.text);
                });
        }
    }

    const {t} = useTranslation("common");
    const checkoutFooter = [
        {
            id: 1,
            name: t("Цена продажи (sale)"),
            price: salePrice,
        },
        // {
        // 	id: 2,
        // 	name: t("text-shipping"),
        // 	price: t("text-free"),
        // },
        {
            id: 2,
            name: t("Всего"),
            price: subtotal,
        },
    ];
    return (
        <div className="pt-12 md:pt-0 2xl:ps-4">
            <h2 className="text-lg md:text-xl xl:text-2xl font-bold text-heading mb-6 xl:mb-8">
                {t("Ваши заказы")}
            </h2>
            <div className="flex p-4 rounded-md mt-6 md:mt-7 xl:mt-9 bg-gray-150 text-sm font-semibold text-heading">
                <span>{t("Товары")}</span>
                {/*<span className="ms-auto flex-shrink-0">{t("text-sub-total")}</span>*/}
            </div>
            {!isEmpty ? (
                items.map((item) => <CheckoutItem item={item} key={item.id}/>)
            ) : (
                <p className="text-red-500 lg:px-3 py-4">{t("Ваша корзина пуста")}</p>
            )}
            {checkoutFooter.map((item: any) => (
                <CheckoutCardFooterItem item={item} key={item.id}/>
            ))}
            <div className="w-full py-3 flex justify-end">
                <Button
                    className="w-full sm:w-auto"
                    onClick={handleSubmitOrder}
                    loading={orderMutate?.isLoading}
                    disabled={orderMutate?.isLoading}
                >
                    {t("Заказать")}
                </Button>
            </div>
            <div className="px-4">
                <h3 className="text-center text-2xl font-semibold mb-2">Наши скидки</h3>
                <table className="w-full text-sm text-left text-gray-500 dark:text-gray-400">
                    <thead
                        className="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                    <tr>
                        <th scope="col" className="px-6 py-3">
                            Сумма продажи
                        </th>
                        <th scope="col" className="px-6 py-3">
                            Процент продажи
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    {salesList?.data?.data?.map((sale: any, index: number) => (
                        <tr className="bg-white border-b dark:bg-gray-800 dark:border-gray-700" key={index}>
                            <th scope="row"
                                className="px-6 py-4 font-medium text-gray-900 dark:text-white whitespace-nowrap">
                                {">="}{sale?.amount} ₽
                            </th>
                            <th scope="row"
                                className="px-6 py-4 font-medium text-gray-900 dark:text-white whitespace-nowrap">
                                {sale?.percent} %
                            </th>
                        </tr>
                    ))}
                    </tbody>
                </table>
            </div>

        </div>
    );
};

export default CheckoutCard;

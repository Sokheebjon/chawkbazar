import {FaPaperPlane, FaLeaf} from "react-icons/fa";
import {GiTrophyCup} from "react-icons/gi";
import {AiFillStar} from "react-icons/ai";
import {IoLogoVk, IoLogoInstagram} from "react-icons/io";

export const footer = {
    widgets: [
        {
            id: 1,
            title: "Доставка по всей России",
            // path: "https://www.instagram.com/redqinc/",
            icon: <FaPaperPlane size={25}/>,
        },
        {
            id: 2,
            title: "100% качественный товар",
            // path: "https://twitter.com/redqinc",
            icon: <GiTrophyCup size={25}/>,
        },
        {
            id: 3,
            title: "100% экологически чистый",
            // path: "https://www.facebook.com/redqinc/",
            icon: <FaLeaf size={25}/>,
        },
        {
            id: 4,
            title: "Низкие цены",
            // path: "https://www.youtube.com/channel/UCjld1tyVHRNy_pe3ROLiLhw",
            icon: <AiFillStar size={25}/>,
        },
    ],
    payment: [
        {
            id: 1,
            path: "https://vk.com/lelokids",
            image: <IoLogoVk size={22}/>,
            name: "payment-master-card",
            width: 34,
            height: 20,
        },
        {
            id: 2,
            path: "https://www.instagram.com/lelokidsopt/",
            image: <IoLogoInstagram size={22}/>,
            name: "payment-master-card",
            width: 34,
            height: 20,
        },
    ],
};

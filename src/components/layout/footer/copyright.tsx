import Container from "@components/ui/container";
// import {useTranslation} from "next-i18next";

interface CopyrightProps {
    payment?: {
        id: string | number;
        path?: string;
        name: string;
        image: any;
        width: number;
        height: number;
    }[];
}

const Copyright: React.FC<CopyrightProps> = ({payment}) => {
    // const {t} = useTranslation("footer");
    return (
        <div className="border-t border-gray-300 pt-5 pb-16 sm:pb-20 md:pb-5 mb-2 sm:mb-0">
            <Container className="flex flex-col-reverse md:flex-row text-center md:justify-between">
                <p className="text-body text-xs lg:text-sm leading-6">
                    Copyright © 2021, Le & Lo Kids, All Rights Reserved.
                </p>

                {payment && (
                    <ul className="hidden md:flex flex-wrap justify-center items-center space-s-4 xs:space-s-5 lg:space-s-7 mb-1 md:mb-0 mx-auto md:mx-0">
                        {payment?.map((item) => (
                            <li
                                className="mb-2 md:mb-0 transition hover:opacity-80"
                                key={`payment-list--key${item.id}`}
                            >
                                <a href={item.path ? item.path : "/#"} target="_blank">
                                    {item.image}
                                </a>
                            </li>
                        ))}
                    </ul>
                )}
            </Container>
        </div>
    );
};

export default Copyright;

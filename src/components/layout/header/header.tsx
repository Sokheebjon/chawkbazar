import React, {useRef} from "react";
import SearchIcon from "@components/icons/search-icon";
import {siteSettings} from "@settings/site-settings";
import HeaderMenu from "@components/layout/header/header-menu";
import Logo from "@components/ui/logo";
import {useUI} from "@contexts/ui.context";
import {addActiveScroll} from "@utils/add-active-scroll";
import dynamic from "next/dynamic";
import {useTranslation} from "next-i18next";
import Cookies from "js-cookie";
// import LanguageSwitcher from "@components/ui/language-switcher";
const AuthMenu = dynamic(() => import("./auth-menu"), {ssr: false});
const CartButton = dynamic(() => import("@components/cart/cart-button"), {
    ssr: false,
});

type DivElementRef = React.MutableRefObject<HTMLDivElement>;
const {site_header} = siteSettings;
const Header: React.FC = () => {
    const {
        openSidebar,
        setDrawerView,
        openModal,
        openSearch,
        setModalView,
        isAuthorized,
    } = useUI();
    const {t} = useTranslation("common");
    const siteHeaderRef = useRef() as DivElementRef;
    addActiveScroll(siteHeaderRef);
    const userString = Cookies.get("user");
    const user = userString ? JSON.parse(userString) : "";

    function handleLogin() {
        setModalView("LOGIN_VIEW");
        return openModal();
    }

    function handleMobileMenu() {
        setDrawerView("MOBILE_MENU");
        return openSidebar();
    }

    return (
        <header
            id="siteHeader"
            ref={siteHeaderRef}
            className="w-full h-16 sm:h-20 lg:h-24 relative z-20"
        >
            <div
                className="innerSticky text-gray-700 body-font fixed bg-white w-full h-16 sm:h-20 lg:h-24 z-20 ps-4 md:ps-0 lg:ps-6 pe-4 lg:pe-6 transition duration-200 ease-in-out">
                <div className="flex items-center justify-center mx-auto max-w-[1920px] h-full w-full">
                    <button
                        aria-label="Menu"
                        className="menuBtn hidden md:flex lg:hidden flex-col items-center justify-center px-5 2xl:px-7 flex-shrink-0 h-full outline-none focus:outline-none"
                        onClick={handleMobileMenu}
                    >
						<span className="menuIcon">
							<span className="bar"/>
							<span className="bar"/>
							<span className="bar"/>
						</span>
                    </button>
                    <Logo/>

                    <HeaderMenu
                        data={site_header.menu}
                        className="hidden lg:flex md:ms-6 xl:ms-10"
                    />

                    {/*<div className="flex-shrink-0 ms-auto lg:me-5 xl:me-8 2xl:me-10">*/}
                    {/*	<LanguageSwitcher />*/}
                    {/*</div>*/}
                    <div className="flex-shrink-0 ms-auto lg:me-5 xl:me-8 2xl:me-10 mr-5">
                        <button
                            className="flex items-center justify-center flex-shrink-0 h-auto relative focus:outline-none transform"
                            onClick={openSearch}
                            aria-label="search-button"
                        >
                            <SearchIcon/>
                        </button>
                    </div>
                    <div
                        className="hidden md:flex justify-end items-center space-s-6 lg:space-s-5 xl:space-s-8 2xl:space-s-10 flex-shrink-0">
                        <div className="-mt-0.5 flex-shrink-0">
                            <AuthMenu
                                // className="text-sm xl:text-base text-heading font-semibold"
                                btnProps={{
                                    className:
                                        "text-sm xl:text-base text-heading font-semibold focus:outline-none",
                                    children: isAuthorized ?
                                        <div className="flex justify-between mr-5">
                                            <img className="inline object-cover w-12 h-12 mr-2 rounded-full"
                                                 src="https://img.myloview.com/stickers/user-icon-vector-male-person-profile-avatar-symbol-in-circle-flat-color-glyph-pictogram-sign-illustration-400-164001750.jpg"
                                                 alt="Avatar"/>
                                            <div className="flex flex-col">
                                                <div className="flex">
                                                    <p className="mr-1 mb-0">{user?.firstName}</p>
                                                    <p>{user?.lastName}</p>
                                                </div>
                                                <span>{user?.email}</span>
                                            </div>
                                        </div> : t("Войти"),
                                    onClick: handleLogin,
                                }}
                            >
                                {t("Войти")}
                            </AuthMenu>
                        </div>
                        <CartButton/>
                    </div>
                </div>
            </div>
        </header>
    );
};

export default Header;

import Link from "@components/ui/link";
import {FaChevronDown} from "react-icons/fa";
import MegaMenu from "@components/ui/mega-menu";
import classNames from "classnames";
import {useTranslation} from "next-i18next";
import {useQuery} from "react-query";
import http from "@framework/utils/http";
import {useState} from "react";

interface MenuProps {
    data: any;
    className?: string;
}

const HeaderMenu: React.FC<MenuProps> = ({className}) => {
    const {t} = useTranslation("menu");
    const [categoryId, setCategoryId] = useState("");
    const headerMenu = useQuery(["get-category-list"], async () => http.get(`/admin/categories`))
    const getSubCategoryList = useQuery(["get-subcategory-list", categoryId], async () => http.get(`/admin/sub-categories/${categoryId}`), {enabled: categoryId !== ""})
    const menuData = (headerMenu?.data?.data || [])?.map((d: any, i: number) => {
        return {
            id: i + 2,
            label: d?.name,
            path: d?._id
        }
    })
    const aboutCompanyHeaders = [
        {
            id: 1,
            path: "/terms-of-cooperation",
            label: "Условия сотрудничества",
        },
        {
            id: 2,
            path: "/delivery-and-payment",
            label: "Доставка и оплата",
        }
    ];
    const handleSubmenuShow = (id: string) => {
        setCategoryId(id);
    }
    const subCategoryData = getSubCategoryList?.data?.data?.map((item: any, index: number) => (
        {
            id: index + 1,
            path: `search?q=${item?._id}`,
            label: item?.name
        }
    ));

    return (
        <nav className={classNames(`headerMenu flex w-full relative`, className)}>
            <div
                className={`menuItem group cursor-pointer py-7`}
            >
                <Link
                    href={"/about-company"}
                    className="inline-flex items-center text-lg text-heading px-3 xl:px-4 py-2 font-normal relative group-hover:text-black"
                >
                    {"О Компании"}
                    <span className="opacity-30 text-xs mt-1 xl:mt-0.5 w-4 flex justify-end">
                        <FaChevronDown
                            className="transition duration-300 ease-in-out transform group-hover:-rotate-180"/>
                    </span>
                </Link>
                <MegaMenu columns={aboutCompanyHeaders}/>
            </div>
            {menuData?.slice(0, 6)?.map((item: any) => (
                <div
                    className={`menuItem group cursor-pointer py-7 text-ellipsis ${
                        item.subMenu ? "relative" : ""
                    }`}
                    key={item.id}
                >
                    <button
                        className="inline-flex items-center text-lg text-heading px-3 xl:px-4 py-2 font-normal relative group-hover:text-black"
                        onMouseOver={() => handleSubmenuShow(item.path)}
                    >
                        {t(item.label)}
                        <span className="opacity-30 text-xs mt-1 xl:mt-0.5 w-4 flex justify-end">
                            <FaChevronDown
                                className="transition duration-300 ease-in-out transform group-hover:-rotate-180"/>
                        </span>
                    </button>

                    {subCategoryData && Array.isArray(subCategoryData) && (
                        <MegaMenu columns={subCategoryData}/>
                    )}
                </div>
            ))}
            <div
                className={`menuItem group cursor-pointer py-7`}
            >
                <Link
                    href={"/contact-us"}
                    className="inline-flex items-center text-lg text-heading px-3 xl:px-4 py-2 font-normal relative group-hover:text-black"
                >
                    {"Контакты"}
                </Link>
            </div>
        </nav>
    );
};

export default HeaderMenu;

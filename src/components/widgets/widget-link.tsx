import type {FC} from "react";
import {useTranslation} from "next-i18next";

interface Props {
    className?: string;
    data: {
        id: number;
        path?: string;
        title: string;
        icon?: any;
    };
}

const WidgetLink: FC<Props> = ({className, data}) => {
    const {title, icon, id} = data;
    const {t} = useTranslation("footer");
    return (
        <div className={`${className}`}>
            <ul className="text-center flex">
                <li
                    key={`widget-list--key${id}`}
                    className="flex items-baseline m-auto text-xl"
                >
                    {icon && (
                        <div className="me-3 relative top-0.5 lg:top-1 text-sm lg:text-base">
                            {icon}
                        </div>
                    )}
                    {/*<Link href={path ? path : "#!"}>*/}
                        <p className="transition-colors duration-200 hover:text-black mb-0">
                            {t(`${title}`)}
                        </p>
                    {/*</Link>*/}
                </li>
            </ul>
        </div>
    );
};

export default WidgetLink;

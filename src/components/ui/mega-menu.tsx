import React from "react";
import Link from "@components/ui/link";
import {useTranslation} from "next-i18next";


type MegaMenuProps = {
    columns: {
        id: number | string;
        path: string,
        label: string,
    }[];
};

const MegaMenu: React.FC<MegaMenuProps> = ({columns}) => {
    const {t} = useTranslation("menu");
    return (
        <div
            className="megaMenu shadow-header bg-gray-200 absolute -start-20 xl:start-0 opacity-0 invisible group-hover:opacity-100 group-hover:visible">
            <div className="grid grid-cols-5">
                {columns?.map((column) => (
                    <ul
                        className="even:bg-gray-150 pb-7 2xl:pb-8 pt-6 2xl:pt-7"
                        key={column.id}
                    >
                        <li className="mb-1.5">
                            <Link
                                href={column.path}
                                className="block text-sm py-1.5 text-heading font-semibold px-5 xl:px-8 2xl:px-10 hover:text-heading hover:bg-gray-300"
                            >
                                {t(column.label)}
                            </Link>
                        </li>
                    </ul>
                ))}
            </div>
        </div>
    );
};

export default MegaMenu;

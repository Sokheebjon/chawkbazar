import Image from "next/image";
import Link from "@components/ui/link";
import cn from "classnames";
import {siteSettings} from "@settings/site-settings";

const Logo: React.FC<React.AnchorHTMLAttributes<{}>> = ({
                                                            className,
                                                            ...props
                                                        }) => {
    return (
        <Link
            href={siteSettings.logo.href}
            className={cn("inline-flex focus:outline-none w-32 text-2xl", className)}
            {...props}

        >
            <style>
                @import
                url('https://fonts.googleapis.com/css2?family=Rubik+Beastly&family=Rubik:wght@600&display=swap');
            </style>
            <Image
                src={siteSettings.logo.url}
                alt={siteSettings.logo.alt}
                height={siteSettings.logo.height}
                width={siteSettings.logo.width}
                layout="fixed"
                loading="eager"
            />
        </Link>
    );
};

export default Logo;

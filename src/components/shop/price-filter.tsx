import {CheckBox} from "@components/ui/checkbox";
import {useRouter} from "next/router";
import React from "react";
import {useTranslation} from "next-i18next";

const priceFilterItems = [
    {
        id: "1",
        name: "Under ₽4 000",
        slug: "0-4000",
    },
    {
        id: "2",
        name: "₽4 000 to ₽8 000",
        slug: "4000-8000",
    },
    {
        id: "3",
        name: "₽8 000 to ₽12 000",
        slug: "8000-12000",
    },
    {
        id: "4",
        name: "₽12 000 to ₽16 000",
        slug: "12000-16000",
    },
    {
        id: "5",
        name: "₽16 000 to ₽20 000",
        slug: "16000-20000",
    },
    {
        id: "6",
        name: "₽20 000 to ₽50 000",
        slug: "20000-50000",
    },
    {
        id: "7",
        name: "₽50 000 to ₽100 000",
        slug: "50000-100000",
    },
    {
        id: "8",
        name: "Over ₽100 000",
        slug: "100000-",
    },
];
export const PriceFilter = () => {
    const {t} = useTranslation("common");
    const router = useRouter();
    const {pathname, query} = router;
    const selectedPrices = query?.price ? (query.price as string).split(",") : [];
    const [formState, setFormState] = React.useState<string[]>(selectedPrices);
    React.useEffect(() => {
        setFormState(selectedPrices);
    }, [query?.price]);

    function handleItemClick(e: React.FormEvent<HTMLInputElement>): void {
        const {value} = e.currentTarget;
        let currentFormState = formState.includes(value)
            ? formState.filter((i) => i !== value)
            : [...formState, value];
        // setFormState(currentFormState);
        const {price, ...restQuery} = query;
        router.push(
            {
                pathname,
                query: {
                    ...restQuery,
                    ...(!!currentFormState.length
                        ? {price: currentFormState.join(",")}
                        : {}),
                },
            },
            undefined,
            {scroll: false}
        );
    }

    const items = priceFilterItems;

    return (
        <div className="block border-b border-gray-300 pb-7 mb-7">
            <h3 className="text-heading text-sm md:text-base font-semibold mb-7">
                {t("Цена")}
            </h3>
            <div className="mt-2 flex flex-col space-y-4">
                {items?.map((item: any) => (
                    <CheckBox
                        key={item.id}
                        label={item.name}
                        name={item.name.toLowerCase()}
                        checked={formState.includes(item.slug)}
                        value={item.slug}
                        onChange={handleItemClick}
                    />
                ))}
            </div>
        </div>
    );
};

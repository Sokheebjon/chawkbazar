import Container from "@components/ui/container";
import Layout from "@components/layout/layout";
import PageHeader from "@components/ui/page-header";
import ContactInfoBlock from "@containers/contact-info";
// import {useTranslation} from "next-i18next";
import {serverSideTranslations} from "next-i18next/serverSideTranslations";
import {GetStaticProps} from "next";

export default function ContactUsPage() {
    // const {t} = useTranslation("common");

    const requisites = [
        {
            officialNameOrganization: "ИНН/КПП",
            limitedLiability: "7702459969/772201001"
        },
        {
            officialNameOrganization: "ОГРН",
            limitedLiability: "5187746025099"
        },
        {
            officialNameOrganization: "Web сайт вашей фирмы",
            limitedLiability: "www.lelokids.ru",
            link: "https://lelokids.ru/"
        },
        {
            officialNameOrganization: "Юридический адрес",
            limitedLiability: "109316, г. Москва, Остаповский проезд , д.5, строение 6, офис 201"
        },
        {
            officialNameOrganization: "Почтовый адрес для Корреспонденции",
            limitedLiability: "121352, г. Москва, а/я 7"
        },
        {
            officialNameOrganization: "Банковские реквизиты",
            colSpan: 2
        },
        {
            officialNameOrganization: "Реквизиты банка",
            limitedLiability: "ООО «ПроКоммерцБанк»"
        },
        {
            officialNameOrganization: "Адрес",
            limitedLiability: "РФ, 127422, г. Москва, ул. Тимирязевская д.1"
        },
        {
            officialNameOrganization: "р/счет",
            limitedLiability: "40702810300000002436"
        },
        {
            officialNameOrganization: "к/счет",
            limitedLiability: "30101810145250000699"
        },
        {
            officialNameOrganization: "БИК",
            limitedLiability: "044525699 в ГУ Банка России по ЦФО"
        },
        {
            space: 25
        },
        {
            officialNameOrganization: "E-mail общий:",
            limitedLiability: "info@lelokids.ru",
            link: "info@lelokids.ru"
        },
        {
            officialNameOrganization: "E-mail бухгалтерии",
            limitedLiability: "buh@lelokids.ru",
            link: "buh@lelokids.ru"
        },
        {
            officialNameOrganization: "E-mail отдела продаж",
            limitedLiability: "zakaz@lelokids.ru",
            link: "zakaz@lelokids.ru"
        },
        {
            officialNameOrganization: "Тел/факс",
            limitedLiability: "+7(495)766-53-56, 8-800-101-90-56"
        },
        {
            officialNameOrganization: "Ген. директор",
            limitedLiability: "Шимшек Мустафа"
        },
        {
            officialNameOrganization: "Гл. бухгалтер",
            limitedLiability: "Джураева Гулбахор Амановна"
        }
    ]


    return (
        <>
            <PageHeader pageHeader="Связаться с нами"/>
            <Container>
                <div
                    className="my-14 lg:my-16 xl:my-20 px-0 pb-2 lg: xl:max-w-screen-xl mx-auto flex flex-col md:flex-row w-full">
                    <div className="md:w-full lg:w-2/5 2xl:w-2/6 flex flex-col h-full">
                        <ContactInfoBlock/>
                    </div>
                    <div className="md:w-full lg:w-3/5 2xl:w-4/6 flex h-full md:ms-7 flex-col lg:ps-7">
                        <div
                            className="px-0 pb-2 lg: xl:max-w-screen-xl mx-auto flex flex-col w-full">
                            <p className="text-xl font-bold pb-5">Реквизиты</p>
                            <table className="table-auto border-collapse border border-black text-center">
                                <thead>
                                <tr>
                                    <th className="border border-black">Официальное наименование организации</th>
                                    <th className="border border-black">Общество с Ограниченной Ответственностью
                                        «ЛЕЛО»
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                {requisites?.map(({
                                                      officialNameOrganization,
                                                      limitedLiability,
                                                      colSpan,
                                                      link,
                                                      space
                                                  }, index) => (
                                    <tr style={{height: space}} key={index}>
                                        <td colSpan={colSpan}
                                            className="border border-black font-bold">{officialNameOrganization}</td>
                                        <td className="border border-black">
                                            <a href={link}>{limitedLiability}</a>
                                        </td>
                                    </tr>
                                ))}
                                </tbody>
                            </table>
                        </div>
                        {/*<div className="flex pb-7 md:pb-9 mt-7 md:-mt-1.5">*/}
                        {/*    <h4 className="text-2xl 2xl:text-3xl font-bold text-heading">*/}
                        {/*        {t("Форма обратной связи")}*/}
                        {/*    </h4>*/}
                        {/*</div>*/}
                        {/*<ContactForm/>*/}
                    </div>
                </div>

            </Container>
        </>
    );
}

ContactUsPage.Layout = Layout;

export const getStaticProps: GetStaticProps = async ({locale}) => {
    return {
        props: {
            ...(await serverSideTranslations(locale!, [
                "common",
                "forms",
                "menu",
                "footer",
            ])),
        },
    };
};

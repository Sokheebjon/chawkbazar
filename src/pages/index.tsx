import BannerCard from '@components/common/banner-card';
import Container from '@components/ui/container';
import BrandGridBlock from '@containers/brand-grid-block';
// import CategoryBlock from '@containers/category-block';
import Layout from '@components/layout/layout';
// import BannerWithProducts from '@containers/banner-with-products';
import BannerBlock from '@containers/banner-block';
import Divider from '@components/ui/divider';
// import DownloadApps from '@components/common/download-apps';
// import Support from '@components/common/support';
// import Instagram from '@components/common/instagram';
import ProductsFlashSaleBlock from '@containers/product-flash-sale-block';
// import ProductsFeatured from '@containers/products-featured';
// import BannerSliderBlock from '@containers/banner-slider-block';
// import ExclusiveBlock from '@containers/exclusive-block';
// import NewArrivalsProductFeed from '@components/product/feeds/new-arrivals-product-feed';
// import {homeThreeBanner as bannerb} from '@framework/static/banner';
import {homeThreeMasonryBanner as masonryBanner} from '@framework/static/banner';
import {serverSideTranslations} from 'next-i18next/serverSideTranslations';
import {ROUTES} from '@utils/routes';
import {GetStaticProps} from 'next';
import Certificate from "@components/certificate/certificate";
import {useQuery} from "react-query";
import http from "@framework/utils/http";

export default function Home() {
    const getBannerList = useQuery(["get-banner-list"], async () => http.get(`/admin/list/banners`))
    const bannerData = getBannerList?.data?.data;
    const banner = {
        id: bannerData?.[6]?.id,
        image: {
            desktop: {
                url: bannerData?.[6]?.photo,
                width: 1800,
                height: 570
            },
            mobile: {
                url: bannerData?.[6]?.photo,
                width: 450,
                height: 180
            }
        },
        slug: bannerData?.[6]?.tag
    }
    return (
        <>
            <BannerBlock data={masonryBanner}/>
            <Container>
                <ProductsFlashSaleBlock/>
            </Container>
            {/*<BannerSliderBlock/>*/}
            <Container>
                {/*<CategoryBlock sectionHeading="text-shop-by-category" type="rounded"/>*/}
                {/*<ProductsFeatured sectionHeading="text-featured-products"/>*/}
                {!getBannerList?.isLoading ?
                    <BannerCard
                        key={`banner--key${banner?.id}`}
                        banner={banner}
                        href={`${ROUTES.SEARCH}?q=boys`}
                        className="mb-12 lg:mb-14 xl:mb-16 pb-0.5 lg:pb-1 xl:pb-0"
                    /> : <svg role="status"
                              className="w-8 h-8 mr-2 text-gray-200 animate-spin dark:text-gray-600 fill-blue-600"
                              viewBox="0 0 100 101" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path
                            d="M100 50.5908C100 78.2051 77.6142 100.591 50 100.591C22.3858 100.591 0 78.2051 0 50.5908C0 22.9766 22.3858 0.59082 50 0.59082C77.6142 0.59082 100 22.9766 100 50.5908ZM9.08144 50.5908C9.08144 73.1895 27.4013 91.5094 50 91.5094C72.5987 91.5094 90.9186 73.1895 90.9186 50.5908C90.9186 27.9921 72.5987 9.67226 50 9.67226C27.4013 9.67226 9.08144 27.9921 9.08144 50.5908Z"
                            fill="currentColor"/>
                        <path
                            d="M93.9676 39.0409C96.393 38.4038 97.8624 35.9116 97.0079 33.5539C95.2932 28.8227 92.871 24.3692 89.8167 20.348C85.8452 15.1192 80.8826 10.7238 75.2124 7.41289C69.5422 4.10194 63.2754 1.94025 56.7698 1.05124C51.7666 0.367541 46.6976 0.446843 41.7345 1.27873C39.2613 1.69328 37.813 4.19778 38.4501 6.62326C39.0873 9.04874 41.5694 10.4717 44.0505 10.1071C47.8511 9.54855 51.7191 9.52689 55.5402 10.0491C60.8642 10.7766 65.9928 12.5457 70.6331 15.2552C75.2735 17.9648 79.3347 21.5619 82.5849 25.841C84.9175 28.9121 86.7997 32.2913 88.1811 35.8758C89.083 38.2158 91.5421 39.6781 93.9676 39.0409Z"
                            fill="currentFill"/>
                    </svg>
                }
                <BrandGridBlock sectionHeading="Самые продаваемые товары"/>
                {/*<BannerCard*/}
                {/*  key={`banner--key${banner[1].id}`}*/}
                {/*  banner={banner[1]}*/}
                {/*  href={`${ROUTES.COLLECTIONS}/${banner[1].slug}`}*/}
                {/*  className="mb-12 lg:mb-14 xl:mb-16 pb-0.5 lg:pb-1 xl:pb-0"*/}
                {/*/>*/}
                {/*<BannerWithProducts*/}
                {/*  sectionHeading="text-on-selling-products"*/}
                {/*  categorySlug="/search"*/}
                {/*/>*/}
                {/*<ExclusiveBlock />*/}
                {/*<NewArrivalsProductFeed />*/}
                {/*<DownloadApps />*/}
                {/*<Support />*/}
                {/*<Instagram />*/}
                {/*<Subscription className="bg-opacity-0 px-5 sm:px-16 xl:px-0 py-12 md:py-14 xl:py-16" />*/}
                <Certificate/>
            </Container>
            <Divider className="mb-0"/>
        </>
    );
}

Home.Layout = Layout;

export const getStaticProps: GetStaticProps = async ({locale}) => {
    return {
        props: {
            ...(await serverSideTranslations(locale!, [
                'common',
                'forms',
                'menu',
                'footer',
            ])),
        },
    };
};

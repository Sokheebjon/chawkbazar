import React from "react";
import Layout from "@components/layout/layout";
import Container from "@components/ui/container";


export default function termsOfCooperation() {
    return (
        <div>
            <Container>
                <div
                    className="my-7 lg:my-7 xl:my-20 px-0 pb-2 lg: xl:max-w-screen-xl mx-auto w-full">
                    <p className="text-2xl font-bold">
                        Условия сотрудничества
                    </p>
                    <p className="text-black">Наша компания, помимо розницы, работает с оптовыми покупателями, а также с
                        крупным оптом на индивидуальных условиях.

                        Продажа осуществляется не только в розницу, но и упаковками или мешками.

                        Любой удобный для вас вариант вы можете обсудить со специалистом отдела продаж.
                    </p>
                </div>
            </Container>
        </div>
    )
}

termsOfCooperation.Layout = Layout

import Container from "@components/ui/container";
import Layout from "@components/layout/layout";
import ProductSingleDetails from "@components/product/product-single-details";
import Divider from "@components/ui/divider";
import Breadcrumb from "@components/common/breadcrumb";
import {serverSideTranslations} from "next-i18next/serverSideTranslations";
import {GetServerSideProps} from "next";
import Head from 'next/head'
import {useQuery} from "react-query";
import http from "@framework/utils/http";
import {useRouter} from "next/router";

export default function ProductPage() {
    const {query} = useRouter();
    const getSingleProduct = useQuery(["get-single-product-by-id"], async () => http.get(`/product/${query?.slug}`), {enabled: !!query?.slug})

    return (
        <>
            <Head>
                <title>{getSingleProduct?.data?.data?.title}</title>
                <meta name="description" content={getSingleProduct?.data?.data?.description}/>
                <meta property="og:title" content="This content about Product page"/>
                <meta
                    property="og:image"
                    content={getSingleProduct?.data?.data?.photos?.[0]}
                />
            </Head>
            <Divider className="mb-0"/>
            <Container>
                <div className="pt-8">
                    <Breadcrumb/>
                </div>
                <ProductSingleDetails/>
                {/*<RelatedProducts sectionHeading="text-related-products" />*/}
            </Container>
        </>
    );
}

ProductPage.Layout = Layout;

export const getServerSideProps: GetServerSideProps = async ({locale}) => {
    return {
        props: {
            ...(await serverSideTranslations(locale!, [
                "common",
                "forms",
                "menu",
                "footer",
            ])),
        },
    };
};

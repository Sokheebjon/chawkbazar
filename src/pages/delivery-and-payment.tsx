import React from "react";
import Layout from "@components/layout/layout";
import Container from "@components/ui/container";


export default function deliveryAndPayment() {
    return (
        <div>
            <Container>
                <div
                    className="my-7 lg:my-7 xl:my-20 px-0 pb-2 lg: xl:max-w-screen-xl mx-auto w-full">
                    <p className="text-2xl font-bold">
                        Доставка и оплата
                    </p>
                    <p className="text-black">ДОСТАВКА & ОПЛАТА</p>
                </div>
            </Container>
        </div>
    )
}

deliveryAndPayment.Layout = Layout;

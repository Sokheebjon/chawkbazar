import axios from "axios";
import Cookies from "js-cookie";

const http = axios.create({
    baseURL: `https://lelokid.herokuapp.com/api`,
    timeout: 30000,
    headers: {
        // Accept: "application/json",
        // "Content-Type": "application/json",
        "auth-user": Cookies.get("auth_token")
    },
});

// Change request data/error here
http.interceptors.request.use(
    (config) => {
        config.headers = {
            ...config.headers,
            "auth-user": Cookies.get("auth_token"),
        };
        return config;
    },
    (error) => {
        return Promise.reject(error);
    }
);

export default http;

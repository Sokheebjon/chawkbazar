import {useUI} from "@contexts/ui.context";
// import {API_ENDPOINTS} from "@framework/utils/api-endpoints";
import http from "@framework/utils/http";
import Cookies from "js-cookie";
import {useMutation} from "react-query";

export interface LoginInputType {
    email: string;
    password: string;
}

async function login(input: LoginInputType) {
    return http.post(`/user/login`, input);
}

export const useLoginMutation = () => {
    const {authorize, closeModal} = useUI();
    return useMutation((input: LoginInputType) => login(input), {
        onSuccess: (data: any) => {
            Cookies.set("auth_token", data?.data?.token);
            Cookies.set("userId", data?.data?.user?._id);
            Cookies.set("user", JSON.stringify(data?.data?.user));
            authorize();
            closeModal();
        },
        onError: (data) => {
            console.log(data, "login error response");
        },
    });
};

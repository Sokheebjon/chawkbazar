import {useUI} from "@contexts/ui.context";
// import { API_ENDPOINTS } from "@framework/utils/api-endpoints";
import http from "@framework/utils/http";
import Cookies from "js-cookie";
import {useMutation} from "react-query";

export interface SignUpInputType {
    email: string;
    password: string;
    firstName: string;
    lastName: string;
    msisdn: string;
}

async function signUp(input: SignUpInputType) {
    return http.post(`/user/register`, input);
    return {
        token: `${input.email}.${input.firstName}.${input.lastName}`.split("").reverse().join(""),
    };
}

export const useSignUpMutation = () => {
    const {authorize, closeModal} = useUI();
    return useMutation((input: SignUpInputType) => signUp(input), {
        onSuccess: (data: any) => {
            Cookies.set("auth_token", data?.data?.token);
            Cookies.set("userId", data?.data?.user?._id);
            Cookies.set("user", JSON.stringify(data?.data?.user));
            authorize();
            closeModal();
        },
        onError: (data) => {
            console.log(data, "login error response");
        },
    });
};

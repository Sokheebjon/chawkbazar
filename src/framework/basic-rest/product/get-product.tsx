import {Product} from "@framework/types";
import http from "@framework/utils/http";
import {useQuery} from "react-query";

export const fetchProduct = async (_slug: string) => {
    const {data} = await http.get(`/product/${_slug}`);
    return data;
};
export const useProductQuery = (slug: string) => {
    return useQuery<Product, Error>(["get-single-product", slug], () =>
        fetchProduct(slug)
    );
};

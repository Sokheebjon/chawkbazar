import {QueryOptionsType, Product} from "@framework/types";
import http from "@framework/utils/http";
import {API_ENDPOINTS} from "@framework/utils/api-endpoints";
import {useQuery} from "react-query";

export const fetchSearchedProducts = async ({queryKey}: any) => {
    const [_key, params] = queryKey;
    const {data} = await http.get(`/search/product`, {params});
    return data;
};
export const useSearchQuery = (options: QueryOptionsType) => {
    return useQuery<Product[], Error>(
        [API_ENDPOINTS.SEARCH, options],
        fetchSearchedProducts
    );
};

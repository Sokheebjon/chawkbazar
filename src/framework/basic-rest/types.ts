import {QueryKey} from "react-query";

export type CollectionsQueryOptionsType = {
    text?: string;
    collection?: string;
    status?: string;
    limit?: number;
};

export type CategoriesQueryOptionsType = {
    text?: string;
    category?: string;
    status?: string;
    limit?: number;
};
export type ProductsQueryOptionsType = {
    type: string;
    text?: string;
    category?: string;
    status?: string;
    limit?: number;
};
export type QueryOptionsType = {
    text?: string;
    title?: string;
    category?: string;
    status?: string;
    limit?: number;
};

export type QueryParamsType = {
    queryKey: QueryKey;
    pageParam?: string;
};
export type Attachment = Array<any>;
export type Category = {
    id: number | string;
    name: string;
    slug: string;
    details?: string;
    image?: Array<string>;
    icon?: string;
    products?: Product[];
    productCount?: number;
};
export type Collection = {
    id: number | string;
    name: string;
    slug: string;
    details?: string;
    image?: Attachment;
    icon?: string;
    products?: Product[];
    productCount?: number;
};
export type Brand = {
    id: number | string;
    name: string;
    slug: string;
    image?: any;
    background_image?: any;
    [key: string]: unknown;
};
export type Tag = {
    id: string | number;
    name: string;
    slug: string;
};
export type Product = {
    id: number | string;
    title: string;
    slug: string;
    price: number;
    quantity: number;
    salePrice?: number;
    image: any;
    sku?: string;
    photos?: Array<string>;
    category?: Category;
    tag?: Tag[];
    meta?: any[];
    description?: string;
    variations?: object;
    [key: string]: unknown;
};
export type OrderItem = {
    id: number | string;
    name: string;
    price: number;
    quantity: number;
};
export type Order = {
    id: string | number;
    name: string;
    slug: string;
    products: OrderItem[];
    total: number;
    tracking_number: string;
    customer: {
        id: number;
        email: string;
    };
    shipping_fee: number;
    payment_gateway: string;
};

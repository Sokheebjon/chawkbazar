import {QueryOptionsType} from "@framework/types";
import http from "@framework/utils/http";
import {API_ENDPOINTS} from "@framework/utils/api-endpoints";
import {useQuery} from "react-query";

export const fetchBrands = async ({queryKey}: any) => {
    const [_key, _params] = queryKey;
    const {data} = await http.get(`/best-selling`);
    return data;
};
export const useBrandsQuery = (options: QueryOptionsType) => {
    return useQuery(
        [API_ENDPOINTS.BRANDS, options],
        fetchBrands
    );
};

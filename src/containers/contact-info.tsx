import {FC} from "react";
import {IoLocationSharp, IoMail, IoCallSharp} from "react-icons/io5";
import Link from "@components/ui/link";
import {useTranslation} from "next-i18next";
import {YMaps, Map, Placemark, SearchControl} from 'react-yandex-maps';

const data = [
    {
        id: 1,
        slug: "/",
        icon: <IoLocationSharp/>,
        name: "Адрес",
        description: "LE & LO KIDS RUSSIA\n" +
            "109316, г.Москва, проезд Остаповский, д.5, стр.6, офис 201\n",
    },
    {
        id: 2,
        slug: "/",
        icon: <IoMail/>,
        name: "Эл. адрес",
        description: "shop@lelokids.ru",
    },
    {
        id: 3,
        slug: "/",
        icon: <IoCallSharp/>,
        name: "Телефонный номер",
        description: "+7 (495) 766-53-56",
    },
];

interface Props {
    image?: HTMLImageElement;
}

const ContactInfoBlock: FC<Props> = () => {
    const {t} = useTranslation("common");
    return (
        <div className="mb-6 lg:border lg:rounded-md border-gray-300 lg:p-7">
            <h4 className="text-2xl md:text-lg font-bold text-heading pb-7 md:pb-10 lg:pb-6 -mt-1">
                {t("Наше местонахождение")}
            </h4>
            {data?.map((item: any) => (
                <div key={`contact--key${item.id}`} className="flex pb-7">
                    <div
                        className="flex flex-shrink-0 justify-center items-center p-1.5 border rounded-md border-gray-300 w-10 h-10">
                        {item.icon}
                    </div>
                    <div className="flex flex-col ps-3 2xl:ps-4">
                        <h5 className="text-sm font-bold text-heading">
                            {t(`${item.name}`)}
                        </h5>
                        <Link href={item.slug} className="text-sm mt-0">
                            {t(`${item.description}`)}
                        </Link>
                    </div>
                </div>
            ))}
            <div>
                <YMaps>
                    <div>
                        <Map width={"100%"} defaultState={{center: [55.72273512738892, 37.70451090000641], zoom: 9}}>
                            <SearchControl
                                // instanceRef={(ref) => {
                                //     if (ref) searchRef.current = ref;
                                // }}
                                options={{
                                    float: "left",
                                    provider: "yandex#search",
                                    size: "large"
                                }}
                            />
                            <Placemark properties={{
                                balloonContentBody: "LE & LO KIDS RUSSIA 109316, г.Москва, проезд Остаповский, д.5, стр.6, офис 201"
                            }} geometry={[55.72273512738892, 37.70451090000641]}/>
                        </Map>
                    </div>
                </YMaps>
            </div>
        </div>
    );
};

export default ContactInfoBlock;
